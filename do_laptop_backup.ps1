# Author: Lukas Toggenburger, HTW Chur: lukas.toggenburger@htwchur.ch
#
# Software license: CC0 1.0 Universal https://creativecommons.org/publicdomain/zero/1.0/
#
# Website: https://github.com/ltog/robocopy-backup

$dst = 'F:\laptop_backup_destination'

function StartRobo([string]$letter) {
	# First, make space on destination with /PURGE but don't copy any (new) files (maximum size 1 bytes with /MAX:1 (can't set to 0))
	Write-Host "Step 1: Deleting files from destination that don't exist in source anymore"
	robocopy D:\ $dst\$letter\ /NP /S /PURGE /MAX:1   /LOG:$dst\laptop_backup_$letter.log /A-:HS /V /R:2 /W:0 /XD "System Volume Information" "`$RECYCLE.BIN" /XF "pagefile.sys"
	# Note: /TEE would also print the outpout, but it makes robocopy very slow ; maybe spawn another process which does the equivalent to 'tail -f', i.e. 'Get-Content myLogfile.log -Wait' ?
	
	if ($lastexitcode -gt 7) { # https://social.technet.microsoft.com/Forums/scriptcenter/en-US/890e1ad1-a55d-4214-b5b6-689a14d9b967/using-if-statements-in-powershell-for-search-robocopy-results?forum=ITCG
		return "fail in step 1: error code = $lastexitcode"
	}
	
	# Copy new files with /MIR (first copy only small files, in case the destination drive hasn't got enough space)
	Write-Host "Step 2: Copying new small files from source to destination"
	robocopy D:\ $dst\$letter\ /NP /MIR /MAX:60000000 /LOG+:$dst\laptop_backup_$letter.log /A-:HS /V /R:2 /W:0 /XD "System Volume Information" "`$RECYCLE.BIN" /XF "pagefile.sys"
	
	if ($lastexitcode -gt 7) { # https://social.technet.microsoft.com/Forums/scriptcenter/en-US/890e1ad1-a55d-4214-b5b6-689a14d9b967/using-if-statements-in-powershell-for-search-robocopy-results?forum=ITCG
		return "fail in step 2: error code = $lastexitcode"
	}
	
	# Copy new big files with /MIR
	Write-Host "Step 3: Copying new big files from source to destination"
	robocopy D:\ $dst\$letter\ /NP /MIR               /LOG+:$dst\laptop_backup_$letter.log /A-:HS /V /R:2 /W:0 /XD "System Volume Information" "`$RECYCLE.BIN" /XF "pagefile.sys"
	
	if ($lastexitcode -gt 7) { # https://social.technet.microsoft.com/Forums/scriptcenter/en-US/890e1ad1-a55d-4214-b5b6-689a14d9b967/using-if-statements-in-powershell-for-search-robocopy-results?forum=ITCG
		return "fail in step 3: error code = $lastexitcode"
	}
	
	attrib -h -s $dst\$letter # see https://syneto.net/knowledgebase/robocopy-sets-attributes-on-destination-folder-to-system-or-hidden/ alternative is to use /A-:HS in robocopy
	
	return "success"
}

function NotifyBackupDone() {
	$timestamp = Get-Date -format yyyy-MM-dd_HH-mm-ss
	$wshell = New-Object -ComObject Wscript.Shell
	$wshell.Popup($timestamp + ": Backup is done.",0,"Backup is done.")
}

function NotifyBackupFailed([string]$errormessage) {
	$timestamp = Get-Date -format yyyy-MM-dd_HH-mm-ss
	$wshell = New-Object -ComObject Wscript.Shell
	$wshell.Popup($timestamp + ": ERROR: Backup FAILED. Error: $errormessage",0,"ERROR: Backup FAILED!")
}

if(Test-Path -Path $dst) { # we're looking at the correct drive letter
	if (Test-Path -Path (Join-Path -Path $dst -childpath "use_b_next")) { # use B next ; TODO: remove duplicated code
		$myreturnvalue = StartRobo b
		$myreturnvalue = $myreturnvalue[-1] # http://stackoverflow.com/a/24630840
		if ($myreturnvalue.CompareTo("success")) {
			NotifyBackupFailed $myreturnvalue
		} else {
			if (Test-Path (Join-Path -Path $dst -childpath "use_b_next")) {
				Remove-Item (Join-Path -Path $dst -childpath "use_b_next")
			}
			Get-Date -format yyyy-MM-dd_HH-mm-ss > (Join-Path -Path $dst -childpath "use_a_next") # writing the timestamp to the file is purely informational
			NotifyBackupDone
		}
	} else { # use A next
		$myreturnvalue = StartRobo a
		$myreturnvalue = $myreturnvalue[-1] # http://stackoverflow.com/a/24630840

		if ($myreturnvalue.CompareTo("success")) {
			NotifyBackupFailed $myreturnvalue
		} else {
			if (Test-Path (Join-Path -Path $dst -childpath "use_a_next")) {
				Remove-Item (Join-Path -Path $dst -childpath "use_a_next")
			}
			Get-Date -format yyyy-MM-dd_HH-mm-ss > (Join-Path -Path $dst -childpath "use_b_next") # writing the timestamp to the file is purely informational
			NotifyBackupDone
		}
	}
} else { # path does not exist
	$wshell = New-Object -ComObject Wscript.Shell
	$wshell.Popup("ERROR: Couldn't run backup: Destination folder not found!",0,"ERROR: Couldn't run backup: Destination folder not found!")
}

#Write-host "Press any key to exit..."
#$wait = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")