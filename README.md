# robocopy-backup #

A simple script to create backups using Robocopy. Two backup destinations (`a` and `b`) are used in alternating order (doubling the required space).

## Usage ##

### Using this script with the task scheduler ###

To run with task scheduler, don't run this script directly but instead call `Powershell.exe` and use the path to this script as argument. Additionally make sure to add the argument `-ExecutionPolicy Bypass` to lower restrictions for (only) this script. You may want to add `-WindowStyle Minimized` or `-WindowStyle Hidden` to start the console window minized or completely hidden.

The resulting call should look like this:

`Powershell.exe -ExecutionPolicy Bypass -WindowStyle Minimized D:\robocopy-backup\do_laptop_backup.ps1`

### Cancelling a running instance of this script ###

You can cancel the script by pressing `Ctrl-C`. This will however not quit the script immediately but will finish copying the current file which may take some time if it is big.

## IMPORTANT NOTES ##

### Setting the destination folder ###

On your destination drive you have to create a directory where both backup copies will be written to. This should be a unique name that you don't use elsewhere to ensure that other directories will not be overwritten in the case where the drive letter of another drive changes into your current drive letter of your backup drive.

You'll most certainly have to adjust the path in the variable `$dst` at the beginning of the file to match your uniquely named destination directory.

### Directory visibility ###

Due to an unfortunate bug in Robocopy the backup destinations (`a` and `b`) may not be visible until the script has finished. If you cancel the script using `Ctrl-C` the destinations may stay hidden. If this is the case run the script again or use the command `attrib -h -s ...` on `a` or `b` to make them visible again.

### Disclaimer ###

This software is provided as-is. Use is at your own risk.
